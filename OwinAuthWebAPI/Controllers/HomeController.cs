﻿using OwinAuthWebAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
namespace OwinAuthWebAPI.Controllers
{
    public class HomeController : ApiController
    {
        private List<Student> students = new List<Student>();
        public HomeController()
        {
            students.Add(new Student { RollNumber = 1, Name = "Amit", ClassName = "UKG" });
            students.Add(new Student { RollNumber = 2, Name = "Sumit", ClassName = "four" });
            students.Add(new Student { RollNumber = 3, Name = "Arnav", ClassName = "LKG" });
            students.Add(new Student { RollNumber = 4, Name = "Gaurav", ClassName = "Two" });
            students.Add(new Student { RollNumber = 5, Name = "Kabya", ClassName = "Three" });
        }
        // GET api/<controller>
        [Authorize]
        public IEnumerable<string> Get()
        {
            return students.Select(x => x.Name);
        }

        // GET api/<controller>/5
        [Authorize(Roles ="Admin")]
        public string Get(int id)
        {
            return students.Where(x => x.RollNumber == id).Select(x => x.Name).SingleOrDefault();
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}