﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwinAuthWebAPI.Models
{
    public class Student
    {
        public int RollNumber { get; set; }
        public string Name { get; set; }
        public string ClassName { get; set; }
    }
}